using JLD2: jldopen, @load

# A representation of reconstructor for file storing with commpressed G
struct ReconstructorFile
    cG::Array{Float64,4}
    len::Int
    θstep::Float64
    l::Array{Int,1}
    σ::Int
end


# Compress G matrix by its symmetry to return a shrinked reconstructor
function compress(rcs::PBasexReconstructor)
    R, _, R′, θ′ = polargrids(rcs.len, rcs.θstep)
    l = rcs.l
    imax = length(R′)
    jmax = length(θ′)
    kmax = length(R)
    jmid = ceil(Int, jmax/2)

    G = rcs.G
    cG = zeros(Float64, imax, jmid, kmax, length(l))  # compressed G
    for lidx in 1:length(l)
        sign = ifelse(iseven(l[lidx]), 1, -1)
        for k in 1:kmax
            kl = (k - 1)*length(l) + lidx
            for j in 1:jmid, i in 1:imax
                ij  = (i - 1)*jmax + j
                ij′ = (i - 1)*jmax + (jmax - j + 1)
                @inbounds cG[i, j, k, lidx] = G[ij, kl]
            end
        end
    end
    ReconstructorFile(cG, rcs.len, rcs.θstep, rcs.l, rcs.σ)
end


# Decompress G matrix to return a reconstructor
function decompress(rcsfile::ReconstructorFile)
    R, _, R′, θ′ = polargrids(rcsfile.len, rcsfile.θstep)
    l = rcsfile.l
    imax = length(R′)
    jmax = length(θ′)
    kmax = length(R)
    jmid = ceil(Int, jmax/2)

    cG = rcsfile.cG
    G = zeros(Float64, imax*jmax, kmax*length(l))
    for lidx in 1:length(l)
        sign = ifelse(iseven(l[lidx]), 1, -1)
        for k in 1:kmax
            kl = (k - 1)*length(l) + lidx
            for j in 1:jmid, i in 1:imax
                ij  = (i - 1)*jmax + j
                ij′ = (i - 1)*jmax + (jmax - j + 1)
                @inbounds g = cG[i, j, k, lidx]
                @inbounds G[ij,  kl] = g
                @inbounds G[ij′, kl] = sign*g
            end
        end
    end
    PBasexReconstructor(G, rcsfile.len, rcsfile.θstep, rcsfile.l, rcsfile.σ)
end


"""
    savercs(path::AbstractString, rcs::PBasexReconstructor)

Save the reconstructor to a file in JLD2 format.

The file can be loaded by [`loadrcs`](@ref)

# Example

```julia-repl
julia> using PBasex

julia> pbasex = PBasexReconstructor(200)   # Build a reconstructor
PBasex.PBasexReconstructor[200x200]: l=[0, 2], σ=2

julia> savercs("pbasex_200.jld2", pbasex)  # Save the reconstructor to a file
```
"""
function savercs(path::AbstractString, rcs::PBasexReconstructor; kwargs...)
    rcsfile = compress(rcs)
    jldopen(path, "w"; compress=true, kwargs...) do file
        file["rcsfile"] = rcsfile
    end
    nothing
end


"""
    loadrcs(path::AbstractString)

Load a reconstructor from a file saved by [`savercs`](@ref).

# Example

```julia-repl
julia> using PBasex

julia> pbasex = loadrcs("pbasex_200.jld2")  # Load the reconstructor
PBasex.PBasexReconstructor[200x200]: l=[0, 2], σ=2

julia> ret = pbasex(img)  # Reconstruct an image
PBasex.Reconstructedimage: raw: 200x200, slice: 200x200
```
"""
function loadrcs(path::AbstractString)
    @load path rcsfile
    decompress(rcsfile)
end
