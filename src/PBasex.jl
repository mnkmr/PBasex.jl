"""
    PBasex
This package provides the functions to reconstruct a 3-dimensional distribution
from the corresponding projected 2-dimensional image using **pBasex** method.
[G. A. Garcia et al., *Rev. Sci. Instrum.* **75**, 2004, 4989-4996.]

# Usage
```julia-repl
julia> using PBasex

julia> pbasex = PBasexReconstructor(400)  # Build a reconstructor
PBasex.PBasexReconstructor[400x400]: l=[2], σ=2

julia> ret = pbasex(img)  # Reconstruct an projection image
PBasex.ReconstructedImage: raw: 600x600, slice: 400x400

julia> ret2 = pbasex(img, [210, 210])  # Shift the center of target region
PBasex.ReconstructedImage: raw: 600x600, slice: 400x400
```

# Save & Load a reconstructor
```julia-repl
julia> using PBasex

julia> pbasex = PBasexReconstructor(400)  # Build a reconstructor
PBasex.PBasexReconstructor[400x400]: l=[2], σ=2

julia> savercs("pbasex_400.jld2", pbasex)

julia> pbasex_loaded = loadrcs("pbasex_400.jld2")
PBasex.PBasexReconstructor[400x400]: l=[2], σ=2
```
"""
module PBasex

export
    PBasexReconstructor,
    savercs,
    loadrcs,
    crop,
    shear,
    average,
    smooth,
    radialdist,
    speeddist,
    energydist,
    anisotropy

using Dierckx: Spline2D
using LinearAlgebra: svd, Diagonal
using IonimageAnalysisCore:
    IonimageAnalysisCore,
    crop,
    shear,
    guesscenter,
    legendre,
    circumscribedsquare,
    shiftpos,
    symmetrize,
    cartesian_to_polar,
    polar_to_cartesian,
    average,
    smooth,
    radialdist,
    speeddist,
    energydist,
    anisotropy,
    arclength,
    nans,
    isinside
using Printf: print, @printf, @sprintf
using ProgressMeter: Progress, next!
using QuadGK: quadgk


"Return a grid of polar coordinate"
function polargrids(len::Integer, θstep::Float64)
    R1 = iseven(len) ? 0.5 : 1.0
    R = R1:1.0:(len - 1)/2
    θ = 0.0:θstep:180.0
    R′ = R  # This line assumes that the whole Newton sphere is captured in the image
    θ′ = θ
    R, θ, R′, θ′
end


"The radial part of the basis function (Eq. 2)"
@inline function f_rad(R::Float64, Rₖ::Float64, σ::Int)
    ΔR = R - Rₖ
    exp(-(ΔR*ΔR)/σ)
end


"The angular part of the basis function (Eq. 2)"
@inline function f_ang(l::Int, cosθ::Float64)
    legendre(l, cosθ)
end


"Return the integrand of Eq. 4"
function integrand(x::Float64, z::Float64, Rₖ::Float64, l::Int, σ::Int)
    x² = x*x
    z² = z*z
    function f(r::Float64)
        r² = r*r
        R = sqrt(z² + r²)
        cosθ = z/R
        r*f_rad(R, Rₖ, σ)*f_ang(l, cosθ)/sqrt(r² - x²)
    end
end


# A slight shift to avoid a singular point at x == r
# FIXME: What would be the appropriate number?
const Δx = 1e-12

"Return a factor of G matrix"
function gfactor(R′, θ′, R, l, σ, Rmax)
    # Set the origin (0, 0) as the root of R′ vector
    # R′ points the vertical top when θ′ = 0°
    x = R′*sind(θ′)
    z = R′*cosd(θ′)
    f = integrand(x, z, R, l, σ)
    g, _ = quadgk(f, abs(x)+Δx, Rmax + 3σ)::Tuple{Float64,Float64}
    g
end


"Compute the elements of G matrix by Eq.4"
function gmatrix(R, R′, θ′, l, σ::Integer, verbose::Bool)
    Rmax = last(R)
    imax = length(R′)
    jmax = length(θ′)
    kmax = length(R)
    # G is symmetric along j if l is even and is anti-symmetric if l is odd
    jmid = ceil(Int, jmax/2)

    p = Progress(kmax*length(l), 1, "Computing basis set...")
    G = zeros(Float64, imax*jmax, kmax*length(l))
    for lidx in 1:length(l)
        sign = iseven(l[lidx]) ? 1 : -1
        for k in 1:kmax
            kl = (lidx - 1)*kmax + k
            for i in 1:imax, j in 1:jmid
                @inbounds g = gfactor(R′[i], θ′[j], R[k], l[lidx], σ, Rmax)
                ij  = (i - 1)*jmax + j
                ij′ = (i - 1)*jmax + (jmax - j + 1)
                @inbounds G[ij,  kl] = 2*g
                @inbounds G[ij′, kl] = sign*2*g
            end
            verbose && next!(p)  # update progress meter
        end
    end
    G
end


"Edit singular values to improve the condition number of G matrix"
function conditioning(U, S, V, tol)
    rank = 0
    for σ in S
        σ < tol && break
        rank += 1
    end
    rank == 0 && ArgumentError(@sprintf("The rank of the assigned matrix is zero with the tolerance %e for singular values.", tol)) |> throw
    rank == length(S) && return U, S, V
    return U[:, 1:rank], S[1:rank], V[:, 1:rank]
end


"""
    balance!(G, len::Int, θstep::Float64, l)

Sanitize the G matrix.

In principle, G[ij, kl] == sign*G[ij′, kl] should be true with an arbitrary
(i, j, k, l). However, there could be a slight numerical error. Take average 
( (G[ij, kl] + sign*G[ij′, kl])/2 ) to eliminate the error.
"""
function balance!(G, R, θ, R′, θ′, l)
    imax = length(R′)
    jmax = length(θ′)
    kmax = length(R)
    jmid = ceil(Int, jmax/2)
    for lidx in 1:length(l)
        sign = ifelse(iseven(l[lidx]), 1, -1)
        for k in 1:kmax
            kl = (lidx - 1)*kmax + k
            for j in 1:jmid, i in 1:imax
                ij  = (i - 1)*jmax + j
                ij′ = (i - 1)*jmax + (jmax - j + 1)
                @inbounds g = (G[ij, kl] + sign*G[ij′, kl])/2
                @inbounds G[ij, kl] = g
                @inbounds G[ij′, kl] = sign*g
            end
        end
    end
end


"Obtaine a 3D distribution in polar coordinate from C matrix and the basis-set functions"
function polarmap(C, R, θ, larray, σ::Int)
    cosθ = cosd.(θ)
    F = zeros(Float64, length(θ), length(R))
    for lidx in eachindex(larray)
        l = larray[lidx]
        for k in eachindex(R)
            c = C[k, lidx]
            Rk = R[k]
            for m in eachindex(R)
                f = f_rad(R[m], Rk, σ)
                for n in eachindex(θ)
                    @inbounds F[n, m] += c*f*f_ang(l, cosθ[n])
                end
            end
        end
    end
    F
end


"Obtain a mirror image"
function mirror(half)
    height, halfwidth = size(half)
    if iseven(halfwidth)
        width = 2*halfwidth
        xc⁻ = halfwidth
        xc⁺ = halfwidth + 1
    else
        width = 2*halfwidth - 1
        xc⁻ = halfwidth
        xc⁺ = halfwidth
    end
    image = Matrix{Float64}(undef, (height, width))
    @inbounds for dx in 0:halfwidth-1, y in 1:height
        z = half[y, dx + 1]
        image[y, xc⁻ - dx] = z
        image[y, xc⁺ + dx] = z
    end
    image
end


"""
    PBasexReconstructor

A callable object to reconstruct a 3D distribution from a projected image by
pBasex method.

Each object has its fixed size and is applicable to a square region of the
image.
"""
struct PBasexReconstructor
    G::Matrix{Float64}
    len::Int
    θstep::Float64
    l::Vector{Int}
    σ::Int
end
function PBasexReconstructor(len::Int, θstep::Float64, l, σ::Int,
                             svtol::Real, verbose::Bool)
    R, θ, R′, θ′ = polargrids(len, θstep)
    l = [0; filter(x -> x > 0, sort(unique(l)))]
    G = gmatrix(R, R′, θ′, l, σ, verbose)
    F = svd(G)
    U, S, V = conditioning(F.U, F.S, F.V, svtol)
    G′ = U*Diagonal(S)*transpose(V)
    balance!(G′, R, θ, R′, θ′, l)
    PBasexReconstructor(G′, len, θstep, l, σ)
end

"""
    pbasex = PBasexReconstructor(len::Int; θstep=2.0, l=[0, 2], σ=2,
                                 svtol=eps(), verbose=true)

# Arguments
- `len`  : the length of an edge of reconstructed square.
- `θstep`: the angular step of polar grid for the reconstruction.
           Note that the unit is degree.
- `l`    : the order of legendre polynomials included in the reconstruction.
           In many cases, it will be even numbers like [2], [2, 4]. The zero-th
           order is always included.
- `svtol`: the tolerance to truncate the singular values of the basis-set
           matrix. Singular values less than the tolerance will be set to zero
           to improve the condition number.
- `verbose`: Show progress bar in basis-set calculation if true.

---

    pbasex(img; base=0.0)
    pbasex(img, center; base=0.0)

Reconstruct a 3D distribution from the projection `img`. It returns a
[`ReconstructedImage`](@ref). `pbasex` is a [`PBasexReconstructor`](@ref).

The center of the image is determined at the centroid of binarized image if omitted.

`base` keyword is employed to set a base-level of the image. The pixels darker
than the base-level will be set as the value. The negative pixels are
eliminated in default. If it is not required, pass `nothing`.

# Example

``` julia-repl
julia> using PBasex

julia> pbasex = PBasexReconstructor(200)
PBasex.PBasexReconstructor[200x200]: l=[2], σ=2

julia> ret = pbasex(img)
PBasex.ReconstructedImage: raw: 200x200, slice: 200x200
```


!!! info "Schematic of geometry"
```
-------------------------------------------------------------
| img                                                       |
|                      len                                  |
|               -----------------                           |
|               |               |                           |
|               |               |                           |
|               |       *       | len                       |
|               |     center    |                           |
|               |    (yc, xc)   |                           |
|               -----------------                           |
|                                                           |
|                 reconstructed                             |
|                     region                                |
|                                                           |
|                                                           |
|                                                           |
-------------------------------------------------------------
```
"""
function PBasexReconstructor(len::Int; θstep=2.0, l=[2], σ=2,
                             svtol=eps(), verbose=true)
    PBasexReconstructor(len, Float64(θstep), l, Int(σ), svtol, verbose)
end

function (rcs::PBasexReconstructor)(image0, center0)
    !isinside(center0, image0) &&
        ArgumentError("The center coordinate $(center0) is outside of the $(size(image0, 1))×$(size(image0, 2)) image.") |> throw

    G = rcs.G
    len = rcs.len
    θstep = rcs.θstep
    l = rcs.l
    σ = rcs.σ

    R, θ, R′, θ′ = polargrids(len, θstep)
    image, center = circumscribedsquare(image0, center0, (len - 1)/2)
    size(image) != (len, len) && ArgumentError("Reconstructed area ($(len)×$(len)) is out of the image.") |> throw
    symmetrized = symmetrize(image, 2)
    ij = length(R′)*length(θ′)
    P = reshape(cartesian_to_polar(symmetrized, center, θ′, R′), ij, :)
    σP² = variance(P)
    W = 1 ./ σP²

    ᵗGW = transpose(G)*Diagonal(W)
    G⁺ = (ᵗGW*G) \ ᵗGW
    C = reshape(G⁺*P, (length(R), length(l)))
    varC = reshape(propagate_variance(σP², G⁺), (length(R), length(l)))
    F = polarmap(C, R, θ, l, σ)
    arr = centerslice(F, len, θstep)
    ReconstructedImage(arr, len, θstep, l, σ, C, varC)
end
(rcs::PBasexReconstructor)(img) = rcs(img, guesscenter(img))

function Base.show(io::IO, ::MIME"text/plain", z::PBasexReconstructor)
    @printf("PBasex.PBasexReconstructor[%d×%d]: l=%s, σ=%d", z.len, z.len, z.l, z.σ)
end


"""
    ReconstructedImage{T} <: AbstractMatrix{T}

Reconstructed image and concomitant information.

See also: [`radialdist`](@ref), [`speeddist`](@ref), [`energydist`](@ref),
          [`anisotropy`](@ref)
"""
struct ReconstructedImage{T<:Real} <: AbstractMatrix{T}
    arr::Matrix{T}
    len::Int
    θstep::Float64
    l::Vector{Int}
    σ::Int
    C::Matrix{Float64}
    varC::Matrix{Float64}

    function ReconstructedImage(arr::AbstractMatrix{T}, len::Integer, θstep::Real, l, σ::Integer, C::AbstractMatrix{T}, varC::AbstractMatrix{T}) where {T<:Real}
        l = [0; filter(x -> x > 0, sort(unique(l)))]
        new{T}(arr, len, θstep, l, σ, C, varC)
    end
end

"Return variance vector assuming that the signal counts obey Poisson statistics"
function variance(I::AbstractMatrix{<:Real})
    all(I .≈ 0) && return ones(eltype(I), length(I))

    # Assuming that the signal counts obey Poisson statistics. If the count is
    # big, practically N > 10, it can be approximated to A normal distribution
    # with the standard deviation σI = √I, where I is the count of the pixel.
    # The weight for a residual is the inverse of the square of σI; w = 1/σI².
    σI² = abs.(I)

    # Consider that any pixel has a non-zero standard deviation σIₘᵢₙ.
    # It is estimated to the square root of the non-zero minimum of σI².
    # This is to avoid to obtain w == Inf.
    σImin² = minimum(filter(x -> x > 0, σI²))
    σI²[σI² .== 0] .= σImin²
    return σI²
end

"calculate diag(G⁺*Diagonal(σP²)*transpose(G⁺))"
function propagate_variance(σP², G⁺)
    # NOTE: The count/pixel should obey Poisson statistics. If the count is big
    #       enough, practically N > 10, it can be approximated to A normal
    #       distribution with the standard deviation √N where N is the count of
    #       the pixel. Hence, the covariance matrix of the projection varP is:
    #           varP = Diagonal(P)
    #       The variances of coefficient matrix C can be obtained as the
    #       diagonal terms of its covariance matrix.
    #           varC = diag(G⁺*varP*transpose(G⁺))
    var = zeros(Float64, (size(G⁺, 1),))
    for j in 1:size(G⁺, 2), i in 1:size(G⁺, 1)
        var[i] += G⁺[i, j]^2*σP²[j]
    end
    var
end

function Base.show(io::IO, ::MIME"text/plain", z::ReconstructedImage)
    print("PBasex.ReconstructedImage:\n")
    @printf("  size     : %d × %d\n", z.len, z.len)
    @printf("  intensity: %9.3e\n", sum(z.arr))
    R, θ = polargrids(z.len, z.θstep)
    @printf("  radii    : %5.1f .. %5.1f\n", first(R), last(R))
    @printf("  θ        : %5.1f .. %5.1f\n", first(θ), last(θ))
    @printf("  θstep    : %5.1f", z.θstep)
end

function centerslice(F, len, θstep)
    R, θ = polargrids(len, θstep)
    halfslice = polar_to_cartesian(F, θ, R, len)
    mirror(halfslice)
end
Base.Matrix(rcsimg::ReconstructedImage) = copy(rcsimg.arr)
Base.Array(rcsimg::ReconstructedImage) = copy(rcsimg.arr)
Base.convert(::Type{T}, rcsimg::ReconstructedImage) where T<:Matrix = convert(T, rcsimg.arr)
Base.convert(::Type{AbstractMatrix{Float64}}, rcsimg::ReconstructedImage) = copy(rcsimg.arr)

# AbstractArray interfaces
Base.size(rcsimg::ReconstructedImage) = size(rcsimg.arr)
Base.getindex(rcsimg::ReconstructedImage, I...) = getindex(rcsimg.arr, I...)
Base.length(rcsimg::ReconstructedImage) = length(rcsimg.arr)
Base.axes(rcsimg::ReconstructedImage) = map(Base.OneTo, size(rcsimg.arr))
Base.strides(rcsimg::ReconstructedImage) = strides(rcsimg.arr)
Base.stride(rcsimg::ReconstructedImage, k::Integer) = stride(rcsimg.arr, k)


"""
    radialdist(rcsimg::ReconstructedImage)

Calculate a radial distribution of a reconstructed image data.
See also: [`PBasexReconstructor`](@ref)
"""
function IonimageAnalysisCore.radialdist(rcsimg::ReconstructedImage)
    C = rcsimg.C
    R, _ = polargrids(rcsimg.len, rcsimg.θstep)
    l = rcsimg.l
    σ = rcsimg.σ
    I = zeros(Float64, length(R))
    for m in eachindex(R)
        for k in eachindex(R)
            @inbounds I[m] += C[k, 1]*f_rad(R[m], R[k], σ)
        end
        @inbounds I[m] *= R[m]^2
    end
    R, I
end


"""
    speeddist(rcsimg::ReconstructedImage, ppm, tof, N)

Compute the speed distribution of a reconstructed image data.
See also: [`PBasexReconstructor`](@ref)
"""
function IonimageAnalysisCore.speeddist(rcsimg::ReconstructedImage, ppm, tof, N)
    r, Ir = radialdist(rcsimg)
    speeddist(r, Ir, ppm, tof, N)
end


"""
    energydist(rcsimg::ReconstructedImage, ppm, tof, N, m, M)

Coumpute the energy distribution of a reconstructed image data.
See also: [`PBasexReconstructor`](@ref)
"""
function IonimageAnalysisCore.energydist(rcsimg::ReconstructedImage, ppm, tof, N, m, M)
    v, Iv = speeddist(rcsimg, ppm, tof, N)
    energydist(v, Iv, m, M)
end


"""
    anisotropy(rcsimg::ReconstructedImage)

Compute the anisotropy parameters of a reconstructed image data.
See also: [`PBasexReconstructor`](@ref)
"""
function IonimageAnalysisCore.anisotropy(rcsimg::ReconstructedImage)
    C = rcsimg.C
    varC = rcsimg.varC
    l = rcsimg.l
    σ = rcsimg.σ
    R, θ, _ = polargrids(rcsimg.len, rcsimg.θstep)
    b    = nans(length(R), length(l)-1)
    berr = nans(length(R), length(l)-1)
    for l in 2:length(l), m in eachindex(R)
        arc = arclength(R[m], extrema(θ)...)

        # Independent `length(θ)` data points are necessary for least-square
        # fitting; however, there are only approximately `2π*r` data points at
        # most in the original Cartesian image I(y, x) at `r`.
        arc <= length(θ) && continue

        b[m, l-1], berr[m, l-1] = bl(C, varC, R[m], R, l, σ)
    end
    R, b, berr, l[2:end]
end

function bl(C, varC, R, Rk, l::Integer, σ)
    # NOTE: here `l` is not an order of Legendre polynomials,
    #       `l` is the index of it
    I = 0.0
    b = 0.0
    Ierr² = 0.0
    berr² = 0.0
    for k in eachindex(Rk)
        f = f_rad(R, Rk[k], σ)
        f² = f^2
        I += C[k, 1]*f
        b += C[k, l]*f
        Ierr² += varC[k, 1]*f²
        berr² += varC[k, l]*f²
    end

    # no signal
    I <= 0 && return NaN, NaN

    R² = R^2
    I *= R²
    b *= R²
    Ierr² *= R²
    berr² *= R²
    return  b/I, sqrt((1/I)^2*berr² + (b/I^2)^2*Ierr²)
end


include("file.jl")


end # module
