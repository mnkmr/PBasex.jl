PBasex.jl
=========

[![Build Status](https://gitlab.com/mnkmr/PBasex.jl/badges/master/build.svg)](https://gitlab.com/mnkmr/PBasex.jl/pipelines)
[![Coverage](https://gitlab.com/mnkmr/PBasex.jl/badges/master/coverage.svg)](https://gitlab.com/mnkmr/PBasex.jl/commits/master)

## Obsoleted!!!

This package is renamed to [**PBasexInversion**](https://gitlab.com/mnkmr/PBasexInversion.jl) and not maintained as `PBasex.jl` anymore. Use `PBasexInversion` package instead.
