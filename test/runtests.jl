using DelimitedFiles
using PBasex
using Test

@testset "Unit tests" begin
    @test PBasex.mirror(
        [1.  0.  0.
         0.  2.  0.
         0.  0.  3.
         0.  4.  0.
         5.  0.  0.]) ==
        [0.  0.  1.  0.  0.
         0.  2.  0.  2.  0.
         3.  0.  0.  0.  3.
         0.  4.  0.  4.  0.
         0.  0.  5.  0.  0.]
    @test PBasex.mirror(
        [1.  0.
         0.  2.
         0.  0.
         0.  4.
         5.  0.]) ==
        [0.  1.  1.  0.
         2.  0.  0.  2.
         0.  0.  0.  0.
         4.  0.  0.  4.
         0.  5.  5.  0.]
end


function test_ReconstructedImage(recons)
    @test recons isa PBasex.ReconstructedImage
    @test size(recons) == size(recons.arr)
    @test getindex(recons, :) == getindex(recons.arr, :)
    @test length(recons) == length(recons.arr)
    @test axes(recons) == axes(recons.arr)
    @test strides(recons) == strides(recons.arr)
    @test stride(recons, 1) == stride(recons.arr, 1)
    @test stride(recons, 2) == stride(recons.arr, 2)

    r, Ir = radialdist(recons)
    @test all(0.0 .<= r .<= 70.01)
    @test length(r) == length(Ir) == 70
    @test 4 ≤ argmax(Ir[36:45]) ≤ 6
    @test 4 ≤ argmax(Ir[46:55]) ≤ 6
    @test 4 ≤ argmax(Ir[56:65]) ≤ 6

    v, Iv = speeddist(recons, 10e3, 1e-6, 2.0)
    @test all(0.0 .<= v .<=  3500.1)
    @test length(v) == length(Iv) == 70
    @test 4 ≤ argmax(Iv[36:45]) ≤ 6
    @test 4 ≤ argmax(Iv[46:55]) ≤ 6
    @test 4 ≤ argmax(Iv[56:65]) ≤ 6

    Et, IEt = energydist(recons, 15, 50, 10e3, 1e-6, 2.0)
    @test all(0.0 .<= Et .<=  131.251)
    @test length(Et) == length(IEt) == 70
    @test 4 ≤ argmax(IEt[36:45]) ≤ 6
    @test 4 ≤ argmax(IEt[46:55]) ≤ 6
    @test 4 ≤ argmax(IEt[56:65]) ≤ 6

    diff(arr, expect) = abs(sum(arr)/length(arr) - expect)
    r, b, berr, l = anisotropy(recons)
    β2 = b[:, 1]
    β2err = berr[:, 1]
    @test size(b, 1) == size(berr, 1) == size(r, 1)
    @test size(b, 2) == size(berr, 2) == size(l, 1)
    @test all(isnan.(β2[1:28]))
    @test all(isnan.(β2err[1:28]))
    @test diff(β2[39:41], -1) < 0.01
    @test diff(β2[49:51],  0) < 0.01
    @test diff(β2[59:61],  2) < 0.01
    @test β2err[40] < 0.1
    @test β2err[50] < 0.1
    @test β2err[60] < 0.1
end


@testset "Reconstruction" begin
    # This image has three arcs (141 x 141)
    #   Arc1: r = 40 px, σ = 2, β = -1
    #   Arc2: r = 50 px, σ = 2, β = 0
    #   Arc3: r = 60 px, σ = 2, β = 2
    raw01 = readdlm(joinpath(@__DIR__, "test01.tsv"))
    len = minimum(size(raw01))
    pBasex = PBasexReconstructor(len)

    # The center coordinate is outside of the image
    @test_throws ArgumentError pBasex(raw01, (1, 1))
    @test_throws ArgumentError pBasex(raw01, (1, 71))
    @test_throws ArgumentError pBasex(raw01, (71, 1))

    # The reconstructed area get out of the image
    @test_throws ArgumentError pBasex(raw01, (70, 70))
    @test_throws ArgumentError pBasex(raw01, (72, 72))

    recons = pBasex(raw01)
    test_ReconstructedImage(recons)
    @test recons isa PBasex.ReconstructedImage
    @test size(Matrix(recons)) == size(recons) == size(raw01)

    recons = pBasex(raw01, (71.0, 71.0))
    test_ReconstructedImage(recons)
    @test recons isa PBasex.ReconstructedImage
    @test size(Matrix(recons)) == size(recons) == size(raw01)

    recons = pBasex(raw01, (71, 71))
    test_ReconstructedImage(recons)
    @test recons isa PBasex.ReconstructedImage
    @test size(Matrix(recons)) == size(recons) == size(raw01)

    recons = pBasex(float.(raw01), (71.0, 71.0))
    test_ReconstructedImage(recons)
    @test recons isa PBasex.ReconstructedImage
    @test size(Matrix(recons)) == size(recons) == size(raw01)

    recons = pBasex(float.(raw01), (71, 71))
    test_ReconstructedImage(recons)
    @test recons isa PBasex.ReconstructedImage
    @test size(Matrix(recons)) == size(recons) == size(raw01)

    @test Matrix(recons) == Array(recons)
    @test Matrix(recons) == convert(Matrix, recons)
    @test Matrix(recons) == convert(Array{Float64,2}, recons)


    # This image has three arcs (140 x 140)
    #   Arc1: r = 40 px, σ = 2, β = -1
    #   Arc2: r = 50 px, σ = 2, β = 0
    #   Arc3: r = 60 px, σ = 2, β = 2
    raw02 = readdlm(joinpath(@__DIR__, "test02.tsv"))
    len = minimum(size(raw02))
    pBasex = PBasexReconstructor(len)

    recons = pBasex(raw02, (70.5, 70.5))
    test_ReconstructedImage(recons)
    @test recons isa PBasex.ReconstructedImage
    @test size(Matrix(recons)) == size(recons) == size(raw02)

    recons = pBasex(float.(raw02), (70.5, 70.5))
    test_ReconstructedImage(recons)
    @test recons isa PBasex.ReconstructedImage
    @test size(Matrix(recons)) == size(recons) == size(raw02)

    pbasexfile = tempname()
    savercs(pbasexfile, pBasex)
    pBasex2 = loadrcs(pbasexfile)

    recons2 = pBasex2(raw02, (70.5, 70.5))
    test_ReconstructedImage(recons2)
    @test recons2 isa PBasex.ReconstructedImage
    @test size(Matrix(recons2)) == size(recons) == size(raw02)
    @test Array(recons2) == Array(recons)
end
